Trac Migration
==============

This is a tool to help us figure out what data can be migrated from Trac to
Gitlab, but also if we want to make any modifications to the data (cleanup or
rewrite rules) while we do the migration.

## Python Dependencies

Please install the following packages using Python's `pip` tool:

- python-gitlab

On Debian this can be done using:

    $ sudo pip3 install --upgrade python-gitlab

## Open Questions

This is my notes for things I am unsure about how to migrate.

### Parent Ticket Information

Some Trac tickets have a parent ticket. Gitlab does not support this. How do we
want to migrate those tickets?

### Priority and Severity

What should we do here?

### References

Should we rewrite #xxx ticket references into #${project}/xxx references?

### Look into _comment1, _comment2, etc. property changes on tickets?

They look weird in the comments: See
https://dip.torproject.org/ahf-admin/tor-trac/issues/29607

### Look into issue with ticket #29607

We crash on this one.

## Done:

### Attachments?

Attachments are being handled now.

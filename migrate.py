#!/usr/bin/env python3
#
# Copyright (c) 2019 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.
#

import configparser
import gitlab
import itertools
import logging
import pprint
import sys
import xmlrpc.client

class Config:
    def __init__(self, filename):
        self._config = configparser.ConfigParser()

        # Set default values.
        self._config["general"] = {
            "log_level": "info",
        }

        self._config["trac"] = {
            "rpc_url": "",
            "username": "",
            "password": "",
        }

        self._config["gitlab"] = {
            "url": "",
            "private_token": "",
        }

        self._config["migration"] = {
            "create_projects": "true",
            "usernames": "",
            "fallback_username": "",
        }

        # Let the config file override the defaults.
        self._config.read(filename)

        # Initialize some values that we cache.
        self._usernames = None

    def migration_create_projects(self):
        return self._config["migration"].getboolean("create_projects")

    def migration_delete_projects(self):
        return self._config["migration"].getboolean("delete_projects")

    def migration_usernames(self):
        if self._usernames is None:
            self._usernames = set(map(lambda username: username.strip(),
                                      self._config["migration"]["usernames"].split(",")))

        return self._usernames

    def migration_fallback_username(self):
        return self._config["migration"]["fallback_username"]

    def projects(self):
        result = {}

        for project in self._config.keys():
            if not project.startswith("project: "):
                continue

            result[project[9:]] = {
                "trac_component": self._config[project]["trac_component"],
                "gitlab_project": None,
            }

        return result

    def log_level(self):
        log_level = self._config["general"]["log_level"]
        numeric_log_level = getattr(logging, log_level.upper(), None)

        if not isinstance(numeric_log_level, int):
            raise Exception("Invalid log level: {}".format(log_level))

        return numeric_log_level

    def trac_rpc_url(self):
        return self._config["trac"]["rpc_url"]

    def trac_username(self):
        return self._config["trac"]["username"]

    def trac_password(self):
        return self._config["trac"]["password"]

    def gitlab_url(self):
        return self._config["gitlab"]["url"]

    def gitlab_private_token(self):
        return self._config["gitlab"]["private_token"]

class TracClient:
    def __init__(self, url, username, password):
        self._client = xmlrpc.client.ServerProxy(url.format(**{
            "username": username,
            "password": password,
        }))

        # for method in self._client.system.listMethods():
        #    print(method)
        #    print(self._client.system.methodHelp(method))
        #    print("")

    def attachments(self, ticket_id):
        logging.info("Checking for attachments for Trac ticket #{}".format(ticket_id))

        result = []

        # 'throughput_cn.pdf', 'Throughput measurements from Chinese probe site as of 2019-04-09', 37728, <DateTime '20190409T20:53:38' at 0x79552aefd0>, 'cohosh'
        for filename, description, size, date, uploader in  self._client.ticket.listAttachments(ticket_id):
            logging.info("Fetching attachment {} ({} bytes) for Trac ticket #{}".format(filename, size, ticket_id))
            content = self._client.ticket.getAttachment(ticket_id, filename)

            result.append({
                "filename": filename,
                "description": description,
                "size": size,
                "date": date,
                "uploader": uploader,
                "data": content.data,
            })

        return result

    def ticket(self, ticket_id):
        logging.info("Fetching Trac ticket #{}".format(ticket_id))
        _, _, _, ticket  = self._client.ticket.get(ticket_id)

        logging.info("Fetching Trac ticket #{} changelog".format(ticket_id))
        events = []

        # The format is: [<DateTime '20190702T18:20:39' at 0x73009c6c18>, 'catalyst', 'sponsor', '', 'Sponsor28-can', 1]
        for timestamp, username, event_type, old_value, new_value, unused in self._client.ticket.changeLog(ticket_id):
            events.append({
                "timestamp": timestamp,
                "username":  username,
                "type": event_type,
                "old_value": old_value,
                "new_value": new_value,
            })

        # We need to group events by timestamp before we can render them.
        changes = []
        for timestamp, events in itertools.groupby(events, lambda event: event["timestamp"]):
            events = list(events)
            username = events[0]["username"]

            changes.append(self._cleanup_event({
                "username": username,
                "timestamp": timestamp,
                "events": events,
            }))

        # Fetch attachments, if any.
        attachments = self.attachments(ticket_id)

        return {
            "actualpoints": self._parse_actual_points(ticket.get("actualpoints", "")),
            "cc":           self._parse_cc(ticket["cc"]),
            "changetime":   ticket["changetime"],
            "component":    ticket.get("component", ""),
            "description":  ticket.get("description", ""),
            "keywords":     self._parse_keywords(ticket.get("keywords", "")),
            "milestone":    ticket.get("milestone", None),
            "owner":        ticket.get("owner", None),
            "parent":       ticket.get("parent", None),
            "points":       ticket.get("points", None),
            "priority":     ticket.get("priority", None),
            "reporter":     ticket.get("reporter", None),
            "resolution":   ticket.get("resolution", None),
            "reviewer":     ticket.get("reviewer", None),
            "severity":     ticket.get("severity", None),
            "sponsor":      self._parse_sponsor(ticket.get("sponsor", "")),
            "status":       ticket.get("status", ""),
            "summary":      ticket.get("summary", ""),
            "time":         ticket["time"],
            "type":         ticket.get("type", None),
            "version":      ticket.get("version", None),
            "changelog":    changes,
            "attachments":  attachments,
        }

    def _cleanup_event(self, event):
        # FIXME(ahf): Can multiple users collide in our groupby()? We sure hope not.
        assert all(map(lambda x: x["username"] == event["username"], event["events"]))

        cleaned_event = {}

        for e in event["events"]:
            if e["type"] == "comment":
                # Comments with no value can be removed.
                if e["new_value"].strip() == "":
                    continue

                assert "comment" not in cleaned_event
                cleaned_event["comment"] = e["new_value"]
            elif e["type"] == "keywords":
                assert "keywords" not in cleaned_event
                old = set(map(lambda x: x.strip(), e["old_value"].split(",")))
                new = set(map(lambda x: x.strip(), e["new_value"].split(",")))

                cleaned_event["keywords"] = {
                    "old": old.difference(new),
                    "new": new.difference(old),
                }


            elif e["type"] == "sponsor":
                assert "sponsor" not in cleaned_event
                cleaned_event["sponsor"] = {
                    "old": self._parse_sponsor(e["old_value"]),
                    "new": self._parse_sponsor(e["new_value"]),
                }
            else:
                assert e["type"] not in cleaned_event
                cleaned_event[e["type"]] = {
                    "old": e["old_value"],
                    "new": e["new_value"],
                }

        return {
            "username":  event["username"],
            "timestamp": event["timestamp"],
            "changes":   cleaned_event,
        }

    def _parse_actual_points(self, data):
        if data == "":
            return None

        return data

    def _parse_cc(self, data):
        if data == "":
            return []

        return list(map(lambda x: x.strip(), data.split(",")))

    def _parse_keywords(self, data):
        if data == "":
            return []

        return list(map(lambda x: x.strip(), data.split(",")))

    def _parse_sponsor(self, data):
        if data == "":
            return None

        return data[len("Sponsor"):]

class Migrator:
    def __init__(self, config, projects, trac_api_client, gitlab_api_client):
        self._config = config
        self._projects = projects
        self._trac_client = trac_api_client
        self._gitlab_client = gitlab_api_client

    def migrate_ticket(self, ticket_id):
        logging.info("Migrating ticket #{} from Trac to Gitlab".format(ticket_id))

        ticket = self._trac_client.ticket(ticket_id)

        gitlab_project = self._lookup_trac_project(ticket["component"])

        if gitlab_project is None:
            logging.info("Unable to find mapping between Trac component {} and Gitlab project".format(ticket["component"]))
            return

        logging.info("Found Gitlab project for Trac component: {} -> {}".format(ticket["component"], gitlab_project))

        project = self._projects[gitlab_project]["gitlab_project"]

        if project is None:
            logging.warn("Missing gitlab_project")
            return

        logging.info("Creating Gitlab {}#{}".format(gitlab_project, ticket_id))
        gitlab_issue = project.issues.create({
            "iid":         ticket_id,
            "title":       ticket["summary"],
            "description": ticket["description"],
            "labels":      self._ticket_labels(ticket),
            "created_at":  str(ticket["time"]),
        })

        # Create notes
        for change in ticket["changelog"]:
            body = self._change_to_note(change)

            if body is None:
                continue

            gitlab_issue.notes.create({
                "body": body,
                "created_at": str(change["timestamp"]),
            })

        # Handle attachments.
        for attachment in ticket["attachments"]:
            logging.info("Uploading Trac ticket #{} file to Gitlab: {}".format(ticket_id, attachment["filename"]))

            upload_filename = "trac-{}-{}-{}".format(ticket_id, attachment["uploader"], attachment["filename"])
            uploaded_file = project.upload(upload_filename, filedata=attachment["data"])

            gitlab_issue.notes.create({
                "body": "**Attachment from Trac**: {}".format(uploaded_file["markdown"]),
                "created_at": str(attachment["date"]),
            })


    def _change_to_note(self, change):
        # Let's see if there is a comment and extract that first.
        comment = ""

        if "comment" in change["changes"]:
            comment = change["changes"]["comment"]
            del change["changes"]["comment"]

        metadata = ""
        metadata_changed = False

        for key, value in change["changes"].items():
            if key == "attachment":
                continue

            metadata_changed = True

            old = value["old"]
            new = value["new"]

            to = "**to**"
            end = ""

            if key == "keywords":
                to = "**deleted**, "
                end = "**added**"

                if type(old) == set:
                    old = ", ".join(old)

                if type(new) == set:
                    new = ", ".join(new)

            if old is None or old is "":
                old = "*N/A*"

            if new is None or new is "":
                new = "*N/A*"

            metadata += "**{}**: {} {} {} {}  \n".format(key.capitalize(), old, to, new, end)

        body = comment

        if metadata_changed:
            body += "\n\n**Trac:**  \n" + metadata

        if body == "":
            return None

        return body

    def _ticket_labels(self, ticket):
        result = ticket["keywords"]

        if ticket["sponsor"] is not None:
            result.append("sponsor-{}".format(ticket["sponsor"]))

        if ticket["type"] is not None:
            result.append(ticket["type"])

        return result

    def _lookup_trac_project(self, component):
        for project, options in self._projects.items():
            if options["trac_component"] == component:
                return project

        return None

    def _filter_username(self, username):
        if username in self._config.migration_usernames():
            return username

        return self._config.migration_fallback_username()

    def _filter_usernames(self, usernames):
        result = set()

        for username in usernames:
            if username in self._config.migration_usernames():
                result.add(username)

        return result

def main():
    if len(sys.argv) < 2:
        print("Usage: {} <config.ini>".format(sys.argv[0]))
        sys.exit(1)

    # FIXME(ahf): Remove ...
    ticket_id = int(sys.argv[2])

    # Read our configuration file.
    config = Config(sys.argv[1])

    # Configure the logging library.
    logging.basicConfig(level=config.log_level(),
                        format="%(asctime)s %(levelname)s: %(message)s",
                        datefmt="%d/%m/%Y %H:%M:%S")

    # Setup our Gitlab API client.
    gitlab_api_client = gitlab.Gitlab(config.gitlab_url(), private_token=config.gitlab_private_token(), api_version=4)

    # Authenticate such that we can figure out our own username.
    gitlab_api_client.auth()

    user = gitlab_api_client.user
    logging.info("Authenticated with Gitlab as '{}'".format(user.username))

    # Setup our Trac API client.
    trac_api_client = TracClient(config.trac_rpc_url(), config.trac_username(), config.trac_password())

    # Cleanup our Gitlab projects and possibly recreate them.
    projects = config.projects()

    # Our user.
    account = gitlab_api_client.users.list(username=user.username)[0]

    for project in account.projects.list():
        if project.name in projects:
            projects[project.name]["gitlab_project"] = project

    for project, options in projects.items():
        logging.info("Found Gitlab project {} in config file".format(project))

        if config.migration_delete_projects():
            logging.info("Deleting Gitlab project {}".format(project))

            try:
                if options["gitlab_project"] is not None:
                    gitlab_api_client.projects.delete(options["gitlab_project"].id)
                else:
                    logging.info("Project {} not found for. Skipping deletion.".format(project))
            except:
                logging.warning("Unable to delete Gitlab project: {}".format(project))

        if config.migration_create_projects():
            logging.info("Creating Gitlab project {}".format(project))
            gitlab_project = gitlab_api_client.projects.create({
                "name": project,
            })

            logging.info("Enabling issue tracker for {}".format(project))
            gitlab_project.issues_enabled = True
            gitlab_project.save()

            options["gitlab_project"] = gitlab_project

    # Setup our Migrator.
    migrator = Migrator(config,
                        projects,
                        trac_api_client,
                        gitlab_api_client)

    migrator.migrate_ticket(ticket_id)

if __name__ == "__main__":
    main()
